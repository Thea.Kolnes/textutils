package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			return " ".repeat(width-text.length()) + text;
		}

		public String flushLeft(String text, int width) {
			return text + " ".repeat(width-text.length());
		}

		public String justify(String text, int width) {
			// Splitter strengen opp til arraylist: "hei du" -> ["hei","du"]
			String[] splitted = text.split(" ");
			ArrayList<String> strList = new ArrayList<>(Arrays.asList(splitted));

			// Finner første og siste ord fordi de bruker flushLeft, flushRight og fjerner fra strList
			String first = strList.get(0);
			strList.remove(first);
			String last = strList.get(strList.size()-1);
			strList.remove(last);

			// Finner width som skal brukes i center() og flush(). F.eks. 15 / 3 (antall ord) = 5
			int spaces = (width / splitted.length);

			// Lager setningen som skal returneres og cases for 1, 2 eller flere ord
			String sentence = "";

			if (splitted.length <= 1) {
				sentence += flushLeft(first, spaces);
			}
			else if (splitted.length == 2) {
				sentence += flushLeft(first, spaces) + flushRight(last, spaces);
			}
			else {
				for (String words : strList) {
					sentence = flushLeft(first, spaces);
					sentence += center(words, spaces);
					sentence += flushRight(last, spaces);
				}
			}
			return sentence;
		}};
		
	// @Test
	// void test() {
	// 	fail("Not yet implemented");
	// }

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("fee  foo", aligner.justify("fee foo", 8));
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));

	}
}
