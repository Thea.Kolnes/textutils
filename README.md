[![pipeline status](https://git.app.uib.no/inf112/23v/textutils/badges/main/pipeline.svg)](https://git.app.uib.no/inf112/23v/textutils/-/commits/main) [![coverage report](https://git.app.uib.no/inf112/23v/textutils/badges/main/coverage.svg)](https://git.app.uib.no/inf112/23v/textutils/-/commits/main)

# INF112 TextUtils (for Øving 1)

* Se [Øving 1](https://git.app.uib.no/inf112/23v/inf112.23v/-/wikis/lab-01-intro/oving1)


## TextAligner
* Edgecase - partall bokstaver og oddetalls lengde.
* Lengden på streng må være kortere enn bredden
* Justify – sørge for at lengden på output tilsvarer lengden på bredden til arket.

## Pond
* teste antall ender som er etter så så mange steg
* teste om det er rett rekkefølge på emoji
* skjekke om timer er konstant, simulering øker fart
* 
